import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mcgill_food/meal.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Food for the McGill\'s',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MyHomePage(title: 'McGill Food'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _randomNumber = Random().nextInt(meals.length);

  void _incrementCounter() {
    setState(() {
      _randomNumber = Random().nextInt(meals.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    final meal = meals[_randomNumber];
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              meal.title,
              style: Theme.of(context).textTheme.headline2,
            ),
            if (meal.url != null)
              FlatButton(
                onPressed: () async {
                  await launch(meal.url);
                },
                child: Text(
                  meal.url,
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Next',
        child: Icon(Icons.refresh),
      ),
    );
  }
}
