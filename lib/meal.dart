import 'package:meta/meta.dart';

class Meal {
  final String title;
  final String url;

  const Meal({@required this.title, this.url});
}

final meals = const [
  Meal(title: 'Walking Tacos'),
  Meal(title: 'Fish and Chips'),
  Meal(title: 'Avacodo Toast'),
  Meal(title: 'Grilled Cheese'),
  Meal(title: 'Frozen Pizza'),
  Meal(title: 'Mac N Cheese'),
  Meal(title: 'Spaghetti'),
  Meal(title: 'Chicken Strips and Fries'),
  Meal(title: 'Tilapia and veggies'),
  Meal(title: 'Fish Tacos'),
  Meal(title: 'Tacos'),
  Meal(title: 'Pancakes'),
  Meal(title: 'Omletes'),
  Meal(title: 'Waffles'),
  Meal(title: 'Scrambled Eggs'),
  Meal(title: 'Nugs and Fries'),
  Meal(title: 'Quinoa Bowls'),
  Meal(title: 'Shrimp and Veggie Kabob'),
  Meal(title: 'Frozen Lasagna'),
  Meal(title: 'Tuna Helper'),
  Meal(title: 'Black Bean Burgers'),
  Meal(title: 'Hot Dogs and Hamburgers'),
  Meal(title: 'Hamburgers'),
  Meal(title: 'Hot Dogs'),
  Meal(title: 'Hot Ham and Cheese'),
  Meal(title: 'Quesadilla'),
  Meal(title: 'Nachoes in oven'),
  Meal(title: 'Grilled Salmon'),
  Meal(title: 'Grilled Tilapia'),
  Meal(title: 'Grilled Mahi'),
  Meal(title: 'Grilled Chicken Breast'),
  Meal(title: 'Chili Soup'),
  Meal(title: 'Wraps'),
  Meal(title: 'Cold Meat Sandwich'),
  Meal(title: 'Egg in Toast (Hole in one)'),
  Meal(title: 'Cereal'),
  Meal(title: 'Pancheros'),
  Meal(title: 'Dominos'),
  Meal(title: 'Chinese'),
  Meal(title: 'Baked Black Bean and Sweet Potato Flautas', url: 'https://peasandcrayons.com/2012/09/baked-black-bean-sweet-potato-flautas.html'),
  Meal(title: 'Protein-Packed Vegetarian Burrito Bowls', url: 'https://www.yummly.com/recipe/Protein-Packed-Vegetarian-Burrito-Bowls-9015437'),
  Meal(title: 'Penne Bake with Spinach and Tomatoes', url: 'https://bevcooks.com/2011/09/penne-bake-with-spinach-and-tomatoes/'),
  Meal(title: 'French Toast', url: 'https://www.foodnetwork.com/recipes/robert-irvine/french-toast-recipe-1951408'),
];
